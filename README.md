# VCamera test

Simple test program using VCamera component for Vaadin Flow
to take picture. Doesn't work on Android. But the CameraView 
sample from Vaadin Cookbook does.

Also has a few other things from Vaadin Cookbook, like Vibrate, Local Storage, that 
might come in handy on mobile.

(Source is just a simpler version of the demo app included in VCamera,
and based on Vaadin Flow 23.1)

Camera and geolocation on Android requires https, so this app run with self-signed
certificates on port 4443. 

The keystore is created via baeldung receipt: https://www.baeldung.com/spring-boot-https-self-signed-certificate

$ keytool -genkeypair -alias baeldung -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore baeldung.p12 -validity 3650

$ keytool -genkeypair -alias baeldung -keyalg RSA -keysize 2048 -keystore baeldung.jks -validity 3650

$ keytool -importkeystore -srckeystore baeldung.jks -destkeystore baeldung.p12 -deststoretype pkcs12

The code configuring rest controller for https is not needed with vaadin, but server.port = 4443 should be set in 
application.properties.

Consider terminating https in the front webserver so that certificates need not be managed in all applications.