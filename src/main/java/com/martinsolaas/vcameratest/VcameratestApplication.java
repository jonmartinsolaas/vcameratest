package com.martinsolaas.vcameratest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VcameratestApplication {

	public static void main(String[] args) {
		SpringApplication.run(VcameratestApplication.class, args);
	}

}
