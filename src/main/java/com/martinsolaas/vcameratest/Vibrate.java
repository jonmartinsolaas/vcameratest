package com.martinsolaas.vcameratest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

@Route("vibrate")
public class Vibrate extends VerticalLayout {

    public Vibrate() {

        boolean android = VaadinSession.getCurrent().getBrowser().isAndroid();
        if (!android) {
            add(new Paragraph(
                    "Note: The Vibration API is only supported on Android at the time when this example was implemented."));
        }

        NumberField timeField = new NumberField("Vibrate time");
        timeField.setSuffixComponent(new Span("ms"));
        timeField.setValue(200d);

        Button vibrateButton = new Button("Vibrate me", event -> {
            getElement().executeJs("navigator.vibrate($0)", timeField.getValue());
        });

        Button patternButton = new Button("Vibrate with a pattern", event -> {
            getElement().executeJs("navigator.vibrate([200, 50, 100, 50, 50])");
        });

        add(timeField, vibrateButton, patternButton);
    }

}
