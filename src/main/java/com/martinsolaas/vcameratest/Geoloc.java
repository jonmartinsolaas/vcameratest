package com.martinsolaas.vcameratest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.vaadin.elmot.flow.sensors.GeoLocation;

@Route("geoloc")
public class Geoloc extends VerticalLayout {

    /**
     * NB this probably only works with https on Android browsers.
     */


    public Geoloc() {
        Button butt = new Button("Locate");
        GeoLocation geoLocation = new GeoLocation();
        geoLocation.setWatch(true);
        geoLocation.setHighAccuracy(true);
        geoLocation.setTimeout(100000);
        geoLocation.setMaxAge(500);
        geoLocation.addValueChangeListener(e -> {
            System.out.println("pos:" + e.getValue().toString());
        });
        add(geoLocation);
        add(butt);
        butt.addClickListener(e -> {
            add(new Label(geoLocation.getValue().toString()));

        });
    }
}
