package com.martinsolaas.vcameratest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.template.Id;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.vcamera.DataReceiver;
import org.vaadin.vcamera.VCamera;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Route("")
public class Cam extends VerticalLayout implements HasDataReceiver {

    /**
     * This needs https to work :-)
     */

    @Id("camera")
    private VCamera camera = new VCamera();

    File latest;

    @Id("snap")
    Button takePicture = new Button("Take picture");

    @Id("preview")
    Button preview = new Button("Preview");

    @Id("image")
    Div imageContainer = new Div();

    public Cam() {
        camera.setReceiver(this);
        Map<String, Object> previewOpts = new HashMap<>();
        previewOpts.put("video", true);

        Map<String, Object> recOpts = new HashMap<>();
        recOpts.put("video", true);
        recOpts.put("audio", true);
        camera.setOptions(previewOpts, recOpts);
        add(camera);
        add(takePicture);
        add(preview);
        add(imageContainer);
        camera.showPreview();

        camera.addFinishedListener(e -> {

            String mime = e.getMime();
            if (mime.contains("image")) {
                setImage();
            } else {
                log.error("no image found in mime");
            }
        });

        takePicture.addClickListener(e -> {
            camera.takePicture();
        });

        preview.addClickListener(e -> {
            camera.showPreview();
        });
    }

    private void setImage() {
        imageContainer.removeAll();
        File file = getLatest();
        if (file != null) {
            InputStreamFactory f = () -> {
                try {
                    return new FileInputStream(file);
                } catch (FileNotFoundException e) {
                }
                return null;
            };
            Image image = new Image(new StreamResource("image", f),
                    "The captured image");
            imageContainer.add(image);
        }
    }
    @Override
    public File getLatest() {
        return latest;
    }

    @Override
    public void setLatest(File file) {
        latest = file;
    }
}
